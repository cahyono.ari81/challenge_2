import java.util.Collections;
import java.util.List;

public class CountField {
    private double mean;
    private double median;
    private double modus;
    private int[] modusArray = new int[10];

    public double getMean() {
        return mean;
    }

    public double getMedian() {
        return median;
    }

    public double getModus() { return modus; }

    public int[] getModusArray() {
        return modusArray;
    }

    public CountField(List<Integer> datas) {
        countMean(datas);
        //Sort Data
        Collections.sort(datas);
        countMedian(datas);
        countModus(datas);
    }

    private void countMean(List<Integer> datas) {
        double total = 0;
        for (int data : datas) {
            total += data;
        }
       this.mean = total / datas.size();
    }
    private void countMedian(List<Integer> datas) {
        double total;
        int datasLength = datas.size();
        if((datasLength % 2) == 0){
            // Rumus Genap
            int getCenterIndex = datasLength/2;
            double val1 = datas.get(getCenterIndex);
            double val2 = datas.get(getCenterIndex + 1);
            total = (val1 + val2) / 2;
        }else{
            // Rumus Ganjil
            total = datas.get(datasLength / 2);
        }
        this.median = total;
    }

    private void countModus(List<Integer> datas) {
        int[] modusArray = new int[10];
        for (int data : datas ) {
            if(data == 1) modusArray[0]++;
            if(data == 2) modusArray[1]++;
            if(data == 3) modusArray[2]++;
            if(data == 4) modusArray[3]++;
            if(data == 5) modusArray[4]++;
            if(data == 6) modusArray[5]++;
            if(data == 7) modusArray[6]++;
            if(data == 8) modusArray[7]++;
            if(data == 9) modusArray[8]++;
            if(data == 10) modusArray[9]++;
        }
        int highValue = 0;
        int indexValue = 0;
        for (int i = 0; i < modusArray.length; i++) {
            if(highValue < modusArray[i]){
                highValue = modusArray[i];
                indexValue = ++i;
            }
        }

        this.modusArray = modusArray;
        this.modus      =  indexValue;
    }

}
