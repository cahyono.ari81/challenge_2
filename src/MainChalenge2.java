import java.io.*;
import java.util.*;

public class MainChalenge2 {
    public static void main(String[] args) throws IOException {
        String path           = "C:/temp/directori/";
        BufferedReader bf     = new BufferedReader(new FileReader(path.concat("data_sekolah.csv")));
        List<Integer> list    = ReadFileCsv.convertToLinkedlist(bf);
        CountField countField = new CountField(list);
        Scanner kb            = new Scanner(System.in);

        printTerminal(path);
        int chooseMenu = kb.nextInt();
        if (chooseMenu == 1){
            writeFile1(path, countField);
        }else if(chooseMenu == 2){
            writeFile2(path, countField);
        }else if(chooseMenu == 3){
            writeFile1(path, countField);
            writeFile2(path, countField);
        }
    }

    private static void writeFile2(String path, CountField countField) throws IOException {
        PrintWriter write = new PrintWriter(new FileWriter(path.concat("data_sekolah_output2.txt")));
        write.println("Berikut Hasil Pengolahan Nilai:");
        write.println("Berikut hasil sebaran data nilai");
        write.println("Mean : "   + countField.getMean());
        write.println("Median : " + countField.getMedian());
        write.println("Modus : "  + countField.getModus());
        write.close();
    }

    private static void writeFile1( String path, CountField countField) throws IOException {
        PrintWriter write = new PrintWriter(new FileWriter(path.concat("data_sekolah_output1.txt")));
        write.println("Berikut Hasil Pengolahan Nilai:");
        write.println("Nilai\t|\tFrekuensi");
        int[] modusArray =  countField.getModusArray();

        int total1to5 = 0;
        for (int i = 1; i <= modusArray.length ; i++) {
            int index = i - 1;
            total1to5 += modusArray[index];
            if(i == 5) write.println("< 6\t|\t" + total1to5);
            else if(i > 5) write.println(i +"\t|\t" + modusArray[index]);
        }
        write.close();
    }

    private static void printTerminal(String path){
        System.out.println("-------------------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("-------------------------------------");
        System.out.println("Letakkan File csv dengan nama data_sekolah di direktori");
        System.out.println("Berikut : " + path);
        System.out.println("1. Generate txt untuk menampilkan modus");
        System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, median");
        System.out.println("3. Generate kedua file");
        System.out.println("0. Exit");
        System.out.print("Pilih Menu : ");
    }
}
