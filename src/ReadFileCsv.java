import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadFileCsv {

    public static List<Integer> convertToLinkedlist(BufferedReader bf) throws IOException {
        List<Integer> listVal1 = new LinkedList<>();
        for (int i = 0; i < 8; i++) {
            String[] datas     = bf.readLine().split(";");
            Pattern pattern = Pattern.compile("\\b([1-9]|10)\\b");//.
            for (String data: datas) {
                Matcher matcher = pattern.matcher(data);
                boolean isMatches = matcher.matches();
                if(isMatches) listVal1.add(Integer.valueOf(data));
            }
        }
        return listVal1;
    }
}
